@extends('layouts.app')

@section('content')
<div class="jumbotron jumbotron-welcome bg-primary">
    <div class="container text-center">
        <h1>Welcome</h1>
        <p>
            Sensory free-market math-bridge chrome computer spook dissident drone concrete neural DIY dead hotdog. Monofilament concrete into tanto neon dolphin corrupted media futurity San Francisco long-chain hydrocarbons.              
        </p>
        <p>
            <a class="btn btn-primary btn-lg">Learn more</a>
        </p>
    </div>
</div>

<section class="section-one">
  <div class="container">
  <div class="row">
    <h1 class="text-center">Optimized for modern smart managers</h1>
    <p class="text-center">
      Geodesic neon tower claymore mine tattoo woman cardboard refrigerator Chiba construct pistol pre-realism. 
    </p>
    <div class="col-sm-4">
      <div class="thumbnail text-center">
        <i class="fa fa-tv fa-5x"></i>
        <div class="caption">
          <h3>Discover</h3>
          
          <p>
            <a href="#" class="btn btn-primary">More</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail text-center">
        <i class="fa fa-desktop fa-5x"></i>
        <div class="caption">
          <h3>Discover</h3>
          
          <p>
            <a href="#" class="btn btn-primary">More</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail text-center">
        <i class="fa fa-laptop fa-5x"></i>
        <div class="caption">
          <h3>Discover</h3>
          
          <p>
            <a href="#" class="btn btn-primary">More</a>
          </p>
        </div>
      </div>
    </div>  

    <div class="text-center">
      <a href="#" class="btn btn-success btn-lg">TAKE THE PRODUCT TOUR</a>
    </div>

  </div>
</div>
</section>

@endsection
