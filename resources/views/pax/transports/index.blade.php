@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials._left_sidebar')
            </div>
            <div class="col-sm-8">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                            </div>
                            <div class="panel-body text-center">
                                <div class="alert alert-warning">
                                    Unconfirmed
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Services</h3>
                            </div>
                            <div class="panel-body">
                                @include('partials.pax._submenu')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Transports</h3>
                            </div>

                            <div class="panel-body">
                                @if($pax->transports()->count() > 0)
                                    <a href="{{ url('dashboard/pax/'.$pax->id.'/transports/new') }}"
                                       class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Name</th>
                                            <th>5 Seat Cost</th>
                                            <th>7 Seat Add</th>
                                            <th colspan="2" class="text-center">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pax->transports as $transport)
                                            <tr>
                                                <td>{{ $transport->id }}</td>
                                                <td>{{ $transport->name }}</td>
                                                <td>{{ $transport->five_seat_cost }}</td>
                                                <td>{{ $transport->seven_seat_cost }}</td>
                                                <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                                <td><a href="#"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                @else
                                    <div class="text-center">
                                        <i class="fa fa-car fa-5x"></i>
                                        <h3>Looks like your guest(s) have no transport to travel with.</h3>
                                        <a href="{{ url('dashboard/pax/'.$pax->id.'/transports/new') }}"
                                           class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                                    </div>

                                @endif

                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
