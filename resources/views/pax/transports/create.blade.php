@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials._left_sidebar')
            </div>
            <div class="col-sm-8">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                            </div>
                            <div class="panel-body text-center">
                                <div class="alert alert-warning">
                                    Unconfirmed
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Services</h3>
                            </div>
                            <div class="panel-body">
                                @include('partials.pax._submenu')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Add Transports</h3>
                            </div>

                            <div class="panel-body">

                                @include('common.errors')

                                {!! Form::open(['url' => 'dashboard/pax/'.$pax->id.'/transports', 'class' => 'form-horizontal']) !!}

                                @include('pax.transports._form')

                                <div class="form-group">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">Create</button>
                                    </div>
                                </div>

                                {!! Form::close() !!}

                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
