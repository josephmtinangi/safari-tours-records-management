<div class="form-group">
    {!! Form::label('transport', 'Transport', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('transport', $transportList, null, ['class' => 'form-control', 'placeholder' => 'Pick a transport']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('period', 'Period', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('period', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('vehicles', 'Vehicles', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('vehicles', null, ['class' => 'form-control']) !!}
    </div>
</div>