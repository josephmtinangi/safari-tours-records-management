<div class="form-group">
    {!! Form::label('extra', 'Extra Service', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('extra', $extraList, null, ['class' => 'form-control', 'placeholder' => 'Pick an extra service']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('uses', 'How many times', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('uses', null, ['class' => 'form-control']) !!}
    </div>
</div>