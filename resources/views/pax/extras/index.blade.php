@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials._left_sidebar')
        </div>
        <div class="col-sm-8">

          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                </div>
                <div class="panel-body text-center">
                  <div class="alert alert-warning">
                    Unconfirmed
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Services</h3>
                </div>
                <div class="panel-body">
                  @include('partials.pax._submenu')
                </div>
              </div>
            </div>
            <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Extra Services</h3>
                    </div>
                    <div class="panel-body">

                      @if($pax->extras()->count() > 0)
                        
                        <a href="{{ url('dashboard/pax/'.$pax->id.'/extras/new') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
                        <br><br>

                        <div class="table-responsive">
                            <table class="table table-hover table-stripped table-bordered">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th colspan="2" class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pax->extras as $extra)
                                  <tr>
                                    <td>{{ $extra->id }}</td>
                                    <td>{{ $extra->name }}</td>
                                    <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                    <td><a href="#"><i class="fa fa-trash"></i></a></td>
                                  </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                      @else

                        <div class="text-center">
                          <i class="fa fa-flash fa-5x"></i>
                          <h3>Nothing here</h3>
                          <a href="{{ url('dashboard/pax/'.$pax->id.'/extras/new') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                        </div>

                      @endif

                    </div>
                </div>            
            </div>
          </div>

        </div>
        <div class="col-sm-2">
          @include('partials._right_sidebar')
        </div>
      </div>
    </div>

@endsection
