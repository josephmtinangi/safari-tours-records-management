<div class="form-group">
    {!! Form::label('accommodation', 'Hotel', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('accommodation', $accommodationList, null, ['class' => 'form-control', 'placeholder' => 'Pick a hotel']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('nights', 'Nights', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('nights', null, ['class' => 'form-control']) !!}
    </div>
</div>