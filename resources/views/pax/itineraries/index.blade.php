@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials._left_sidebar')
            </div>
            <div class="col-sm-8">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                            </div>
                            <div class="panel-body text-center">
                                <div class="alert alert-warning">
                                    Unconfirmed
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Services</h3>
                            </div>
                            <div class="panel-body">
                                @include('partials.pax._submenu')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Itineraries</h3>
                            </div>
                            <div class="panel-body">

                                @if($pax->itineraries->count() > 0)

                                    <a href="{{ url('dashboard/pax/'.$pax->id.'/itineraries/new') }}"
                                       class="btn btn-primary pull-right"><i class="fa fa-plus"></i> New</a>
                                    <br><br>

                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Status</th>
                                                <th>Generated</th>
                                                <th><i class="fa fa-file-text-o"></i></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($pax->itineraries as $itinerary)
                                                <tr>
                                                    <td>{{ $itinerary->id }}</td>
                                                    <td>
                                                        @if($itinerary->status)
                                                            Confirmed
                                                        @else
                                                            Pending
                                                        @endif
                                                    </td>
                                                    <td>{{ $itinerary->created_at->diffForHumans() }}</td>
                                                    <td>
                                                        <a href="{{ url('dashboard/pax/'.$pax->id.'/itineraries/' . $itinerary->id) }}">Details</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                @else

                                    <div class="text-center">
                                        <i class="fa fa-file-text-o fa-5x"></i>
                                        <h3>Nothing here</h3>
                                        <a href="{{ url('dashboard/pax/'.$pax->id.'/itineraries/new') }}"
                                           class="btn btn-primary"><i class="fa fa-plus"></i> New</a>
                                    </div>

                                @endif

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
