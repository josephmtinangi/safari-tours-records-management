@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials._left_sidebar')
            </div>
            <div class="col-sm-8">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                            </div>
                            <div class="panel-body text-center">
                                <div class="alert alert-warning">
                                    Unconfirmed
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Services</h3>
                            </div>
                            <div class="panel-body">
                                @include('partials.pax._submenu')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">

                        {!! Form::open(['url' => 'dashboard/pax/'.$pax->id.'/itinerary', 'class' => 'form-horizontal']) !!}

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Itinerary</h3>
                            </div>
                            <div class="panel-body">

                                @include('common.errors')

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>{{ $pax->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Adult</th>
                                            <td>
                                                {!! Form::number('adults', null, ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Child</th>
                                            <td>
                                                {!! Form::number('children', null, ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Duration Days</th>
                                            <td>
                                                {!! Form::number('duration_days', null, ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Duration Nights</th>
                                            <td>
                                                {!! Form::number('duration_nights', null, ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="status" value="1"> Confirmed
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="status" value="0" checked> Pending
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Destinations</th>
                                            <td>
                                                @if($pax->parks()->count() > 0)
                                                    <ul>
                                                        @foreach($pax->parks as $park)
                                                            <li>{{ $park->name }}</li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    <span class="text-danger">
                                                        * No destinations
                                                    </span>
                                                    <a href="{{ url('dashboard/pax/'.$pax->id.'/parks/new') }}"
                                                       class="btn btn-link">Add</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Activities</th>
                                            <td>Game drive / Wildlife views</td>
                                        </tr>
                                        <tr>
                                            <th>Accommodation</th>
                                            <td>
                                                @if($pax->accommodations()->count() > 0)
                                                    <ul>
                                                        @foreach($pax->accommodations as $accommodation)
                                                            <li>{{ $accommodation->name }}</li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    <span>* No accommodation</span>
                                                    <a href="{{ url('dashboard/pax/'.$pax->id.'/accommodations/new') }}" class="btn btn-link">Add</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Start</th>
                                            <td>Arusha</td>
                                        </tr>
                                        <tr>
                                            <th>End</th>
                                            <td>Arusha</td>
                                        </tr>
                                        <tr>
                                            <th>Date Starts</th>
                                            <td>
                                                {!! Form::date('date_starts', null, ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Date Ends</th>
                                            <td>
                                                {!! Form::date('date_ends', null, ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Safari type:</th>
                                            <td>Private</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="panel-footer">
                                <div class="text-center">
                                    {!! Form::submit('Generate', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
