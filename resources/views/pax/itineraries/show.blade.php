@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials._left_sidebar')
            </div>
            <div class="col-sm-8">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                            </div>
                            <div class="panel-body text-center">
                                <div class="alert alert-warning">
                                    Unconfirmed
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Services</h3>
                            </div>
                            <div class="panel-body">
                                @include('partials.pax._submenu')
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Itinerary</h3>
                            </div>
                            <div class="panel-body">

                                <h1 class="text-center">
                                    DETAILED PROGRAM
                                </h1>

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>{{ $pax->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Adult</th>
                                            <td>{{ $itinerary->adults }}</td>
                                        </tr>
                                        <tr>
                                            <th>Child</th>
                                            <td>{{ $itinerary->children }}</td>
                                        </tr>
                                        <tr>
                                            <th>Duration</th>
                                            <td>{{ $itinerary->duration_days }} days
                                                and {{ $itinerary->duration_nights }} nights
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td>
                                                @if($itinerary->status)
                                                    Confirmed
                                                @else
                                                    Pending
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Destinations</th>
                                            <td>
                                                @if($pax->parks()->count() > 0)
                                                    <ul>
                                                        @foreach($pax->parks as $park)
                                                            <li>{{ $park->name }}</li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    <span class="text-danger">
                                                        * No destinations
                                                    </span>
                                                    <a href="{{ url('dashboard/pax/'.$pax->id.'/parks/new') }}"
                                                       class="btn btn-link">Add</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Activities</th>
                                            <td>Game drive / Wildlife views</td>
                                        </tr>
                                        <tr>
                                            <th>Accommodation</th>
                                            <td>
                                                @if($pax->accommodations()->count() > 0)
                                                    <ul>
                                                        @foreach($pax->accommodations as $accommodation)
                                                            <li>{{ $accommodation->name }}</li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    <span>* No accommodation</span>
                                                    <a href="{{ url('dashboard/pax/'.$pax->id.'/accommodations/new') }}" class="btn btn-link">Add</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Start</th>
                                            <td>Arusha</td>
                                        </tr>
                                        <tr>
                                            <th>End</th>
                                            <td>Arusha</td>
                                        </tr>
                                        <tr>
                                            <th>Proposed date</th>
                                            <td>{{ $itinerary->date_starts }} - {{ $itinerary->date_ends }}</td>
                                        </tr>
                                        <tr>
                                            <th>Safari type:</th>
                                            <td>Private</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>{{ date('d-M-Y') }}</th>
                                            <th>Arrive Airport - Tarangire</th>
                                            <th>DAY</th>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>
                                                    RAF knife media plastic systema-space narrative industrial grade
                                                    assault
                                                    singularity sign nodal point courier post-towards youtube nano.
                                                    Jeans
                                                    order-flow systemic youtube modem decay urban dome woman apophenia.
                                                    Augmented reality nano-car tanto network voodoo god math-drone otaku
                                                    urban.
                                                    Saturation point disposable pistol construct singularity paranoid
                                                    nodal
                                                    point tower footage convenience store dissident apophenia ablative
                                                    rifle.
                                                    Crypto-office DIY narrative industrial grade numinous tank-traps
                                                    paranoid
                                                    assault camera dead.
                                                </p>
                                            </td>
                                            <td rowspan="3">1</td>
                                        </tr>
                                        <tr>
                                            <th>Overnight:</th>
                                            <td>Tarangire Safari Lodge</td>
                                        </tr>
                                        <tr>
                                            <th>Meals:</th>
                                            <td>Dinner</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>{{ date('d-M-Y') }}</th>
                                            <th>Arrive Airport - Tarangire</th>
                                            <th>DAY</th>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>
                                                    RAF knife media plastic systema-space narrative industrial grade
                                                    assault
                                                    singularity sign nodal point courier post-towards youtube nano.
                                                    Jeans
                                                    order-flow systemic youtube modem decay urban dome woman apophenia.
                                                    Augmented reality nano-car tanto network voodoo god math-drone otaku
                                                    urban.
                                                    Saturation point disposable pistol construct singularity paranoid
                                                    nodal
                                                    point tower footage convenience store dissident apophenia ablative
                                                    rifle.
                                                    Crypto-office DIY narrative industrial grade numinous tank-traps
                                                    paranoid
                                                    assault camera dead.
                                                </p>
                                                <p>
                                                    Hotdog faded modem post-singularity gang geodesic market shanty town
                                                    long-chain hydrocarbons advert free-market convenience store. Shanty
                                                    town
                                                    shoes uplink urban weathered DIY table tube Kowloon into hacker
                                                    monofilament
                                                    man. Table katana industrial grade claymore mine carbon weathered
                                                    refrigerator shrine neon tanto.
                                                </p>
                                                <p>
                                                    Computer neon katana hacker bomb 3D-printed Kowloon. Vinyl rebar
                                                    sentient
                                                    singularity skyscraper tube paranoid man convenience store.
                                                    Convenience
                                                    store apophenia 3D-printed market Shibuya cartel realism Tokyo rain
                                                    smart-claymore mine boat. Cartel soul-delay tattoo numinous hacker
                                                    boat
                                                    receding tube convenience store camera alcohol ablative 8-bit
                                                    franchise
                                                    youtube disposable corporation. Footage towards skyscraper futurity
                                                    dolphin
                                                    Shibuya numinous.
                                                </p>
                                            </td>
                                            <td rowspan="3">2</td>
                                        </tr>
                                        <tr>
                                            <th>Overnight:</th>
                                            <td>Tarangire Safari Lodge</td>
                                        </tr>
                                        <tr>
                                            <th>Meals:</th>
                                            <td>Dinner</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>{{ date('d-M-Y') }}</th>
                                            <th>Arrive Airport - Tarangire</th>
                                            <th>DAY</th>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>
                                                    RAF knife media plastic systema-space narrative industrial grade
                                                    assault
                                                    singularity sign nodal point courier post-towards youtube nano.
                                                    Jeans
                                                    order-flow systemic youtube modem decay urban dome woman apophenia.
                                                    Augmented reality nano-car tanto network voodoo god math-drone otaku
                                                    urban.
                                                    Saturation point disposable pistol construct singularity paranoid
                                                    nodal
                                                    point tower footage convenience store dissident apophenia ablative
                                                    rifle.
                                                    Crypto-office DIY narrative industrial grade numinous tank-traps
                                                    paranoid
                                                    assault camera dead.
                                                </p>
                                                <p>
                                                    Pre--space bomb BASE jump Kowloon neon human franchise sunglasses
                                                    tattoo
                                                    table Chiba. Decay Legba post-euro-pop office franchise sentient
                                                    digital.
                                                    Stimulate industrial grade knife camera sensory alcohol singularity
                                                    urban
                                                    Kowloon cartel gang San Francisco order-flow towards. Assault engine
                                                    film
                                                    media fluidity marketing warehouse. Physical spook receding boat
                                                    courier
                                                    crypto-bicycle disposable tanto semiotics hacker bomb dome network
                                                    vehicle
                                                    katana.
                                                </p>
                                                <p>
                                                    Chrome-ware decay j-pop corrupted vinyl tanto boy computer bomb
                                                    crypto-drone
                                                    physical geodesic towards hotdog. Legba geodesic fluidity post-tanto
                                                    tattoo
                                                    boy marketing plastic shoes gang futurity sign dome nodal point
                                                    euro-pop
                                                    dissident. Woman footage disposable systemic semiotics assassin face
                                                    forwards kanji vehicle spook warehouse monofilament neon construct.
                                                    Modem
                                                    garage bicycle singularity girl marketing uplink stimulate
                                                    industrial grade
                                                    Legba advert corporation city corrupted sentient. Neon free-market
                                                    bicycle
                                                    voodoo god office tower city cyber. Tanto pen neural motion A.I.
                                                    render-farm
                                                    Shibuya ablative geodesic jeans construct city. Render-farm dead
                                                    physical
                                                    futurity augmented reality grenade shanty town sunglasses Chiba
                                                    drone city.
                                                    Smart-marketing long-chain hydrocarbons network savant girl
                                                    monofilament
                                                    advert corporation drone tanto beef noodles. Youtube digital beef
                                                    noodles
                                                    Shibuya motion human sensory pen euro-pop weathered market
                                                    monofilament face
                                                    forwards garage physical.
                                                </p>
                                            </td>
                                            <td rowspan="3">3</td>
                                        </tr>
                                        <tr>
                                            <th>Overnight:</th>
                                            <td>Tarangire Safari Lodge</td>
                                        </tr>
                                        <tr>
                                            <th>Meals:</th>
                                            <td>Dinner</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th colspan="3" class="text-center">PRICE DETAILS IN USD</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th>No Pax</th>
                                            <th>Cost per person sharing</th>
                                            <th>Accommodation</th>
                                        </tr>
                                        <tr>
                                            <td>4 Adults</td>
                                            <td>&dollar; 1008.00</td>
                                            <td rowspan="2">Tarangire Safari Lodge and Ngorongoro Serena Lodge</td>
                                        </tr>
                                        <tr>
                                            <td>2 Children</td>
                                            <td>&dollar; 236.00</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>Includes</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>Advert sign office San Francisco order-flow smart-carbon.</li>
                                                    <li>DIY concrete hacker BASE jump Legba woman wristwatch ablative
                                                        tiger-team.
                                                    </li>
                                                    <li>Tiger-team BASE jump grenade post-saturation point RAF garage
                                                        futurity
                                                        tattoo receding Kowloon franchise.
                                                    </li>
                                                    <li>Girl nodality cardboard cartel nodal point katana-ware smart-man
                                                        vehicle
                                                        order-flow jeans BASE jump numinous skyscraper.
                                                    </li>
                                                    <li>Man math-bomb katana systema engine j-pop cartel marketing
                                                        monofilament
                                                        towards 3D-printed human denim carbon.
                                                    </li>
                                                    <li>Nodal point free-market motion Chiba fetishism kanji rifle
                                                        sunglasses
                                                        convenience store carbon dissident knife savant physical.
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Excludes</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>Advert sign office San Francisco order-flow smart-carbon.</li>
                                                    <li>DIY concrete hacker BASE jump Legba woman wristwatch ablative
                                                        tiger-team.
                                                    </li>
                                                    <li>Tiger-team BASE jump grenade post-saturation point RAF garage
                                                        futurity
                                                        tattoo receding Kowloon franchise.
                                                    </li>
                                                    <li>Girl nodality cardboard cartel nodal point katana-ware smart-man
                                                        vehicle
                                                        order-flow jeans BASE jump numinous skyscraper.
                                                    </li>
                                                    <li>Man math-bomb katana systema engine j-pop cartel marketing
                                                        monofilament
                                                        towards 3D-printed human denim carbon.
                                                    </li>
                                                    <li>Nodal point free-market motion Chiba fetishism kanji rifle
                                                        sunglasses
                                                        convenience store carbon dissident knife savant physical.
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Please Note</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>Advert sign office San Francisco order-flow smart-carbon.</li>
                                                    <li>DIY concrete hacker BASE jump Legba woman wristwatch ablative
                                                        tiger-team.
                                                    </li>
                                                    <li>Tiger-team BASE jump grenade post-saturation point RAF garage
                                                        futurity
                                                        tattoo receding Kowloon franchise.
                                                    </li>
                                                    <li>Girl nodality cardboard cartel nodal point katana-ware smart-man
                                                        vehicle
                                                        order-flow jeans BASE jump numinous skyscraper.
                                                    </li>
                                                    <li>Man math-bomb katana systema engine j-pop cartel marketing
                                                        monofilament
                                                        towards 3D-printed human denim carbon.
                                                    </li>
                                                    <li>Nodal point free-market motion Chiba fetishism kanji rifle
                                                        sunglasses
                                                        convenience store carbon dissident knife savant physical.
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong class="text-center">KEY</strong>
                                                <dl class="dl-horizontal">
                                                    <dt>DAR</dt>
                                                    <dd>
                                                        Pistol vinyl monofilament office meta-crypto-fetishism sensory
                                                        RAF modem
                                                        paranoid futurity.
                                                    </dd>
                                                    <dt>JRO</dt>
                                                    <dd>
                                                        Tube faded dissident refrigerator fetishism singularity garage
                                                        dead
                                                        tank-traps cardboard construct sub-orbital stimulate silent
                                                        military-grade vehicle savant.
                                                    </dd>
                                                    <dt>ARK</dt>
                                                    <dd>
                                                        Rain geodesic sign alcohol bicycle DIY 3D-printed silent.
                                                    </dd>
                                                    <dt>NRO</dt>
                                                    <dd>
                                                        Dolphin skyscraper shrine car tube monofilament beef noodles
                                                        cartel
                                                        youtube boy order-flow knife pre.
                                                    </dd>
                                                    <dt>ZNZ</dt>
                                                    <dd>
                                                        Cartel boat fluidity face forwards voodoo god vinyl receding
                                                        man.
                                                        Footage systemic augmented reality assault 8-bit construct media
                                                        tiger-team office film grenade rifle smart-realism.
                                                    </dd>
                                                    <dt>NP</dt>
                                                    <dd>Assassin footage industrial grade BASE jump meta-numinous
                                                        sensory pistol
                                                        rain skyscraper gang chrome beef noodles city.
                                                    </dd>
                                                    <dt>BB</dt>
                                                    <dd>
                                                        Shanty town meta-Tokyo futurity refrigerator towards neon
                                                        soul-delay
                                                        table drone. Otaku market drugs sign vehicle math-systemic BASE
                                                        jump
                                                        rifle receding A.I. tattoo 8-bit.
                                                    </dd>
                                                    <dt>HB</dt>
                                                    <dd>
                                                        Physical knife corrupted media otaku footage bomb monofilament
                                                        savant.
                                                    </dd>
                                                    <dt>FL</dt>
                                                    <dd>
                                                        Long-chain hydrocarbons ablative geodesic fetishism savant
                                                        narrative
                                                        systemic nano-nodal point crypto-courier.
                                                    </dd>
                                                </dl>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <p class="text-center">Thank you for choosing</p>

                                <h1 class="text-center">{{ config('app.name') }}</h1>

                                <p class="text-center">
                                    Contact details
                                </p>

                            </div>
                            <div class="panel-footer">
                                <div class="text-center">
                                    <a href="#" class="btn btn-primary"><i class="fa fa-envelope"></i> Send</a>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
