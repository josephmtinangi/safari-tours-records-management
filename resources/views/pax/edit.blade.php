@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials._left_sidebar')
        </div>
        <div class="col-sm-8">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title text-center">Edit {{ $pax->name }}</h3>
            </div>
            <div class="panel-body">

                {!! Form::model($pax, ['url' => 'dashboard/pax/' . $pax->id, 'class' => 'form-horizontal']) !!}

                {{ method_field('patch')  }}

                @include('pax._form')

                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
          </div>

        </div>
        <div class="col-sm-2">
          @include('partials._right_sidebar')
        </div>
      </div>
    </div>

@endsection
