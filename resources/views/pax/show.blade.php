@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials._left_sidebar')
        </div>
        <div class="col-sm-8">

          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                </div>
                <div class="panel-body text-center">
                  <div class="alert alert-warning">
                    Unconfirmed
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Services</h3>
                </div>
                <div class="panel-body">
                  @include('partials.pax._submenu')
                </div>
              </div>
            </div>
            <div class="col-sm-9">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Profile</h3>
                </div>
                <div class="panel-body">
                  <dl class="dl-horizontal">
                    <dt>Name</dt>
                    <dd>{{ $pax->name }}</dd>
                    <dt>Email</dt>
                    <dd>{{ $pax->email }}</dd>
                    <dt>Phone</dt>
                    <dd>{{ $pax->phone }}</dd>
                    <dt>Country</dt>
                    <dd>{{ $pax->nationality }}</dd>
                    <dt>Added</dt>
                    <dd>{{ $pax->created_at->diffForHumans() }}</dd>
                    <dt>Last Updated</dt>
                    <dd>{{ $pax->updated_at->diffForHumans() }}</dd>
                  </dl>
                </div>
              </div>              
            </div>
          </div>

        </div>
        <div class="col-sm-2">
          @include('partials._right_sidebar')
        </div>
      </div>
    </div>

@endsection
