@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials._left_sidebar')
        </div>
        <div class="col-sm-8">

          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                </div>
                <div class="panel-body text-center">
                  <div class="alert alert-warning">
                    Unconfirmed
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Services</h3>
                </div>
                <div class="panel-body">
                  @include('partials.pax._submenu')
                </div>
              </div>
            </div>
            <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Invoice</h3>
                    </div>
                    <div class="panel-body">

                        <h1 class="text-center">INVOICE</h1>
                        <h2 class="text-center">TIN: 0123456789</h2>

                        <div class="table-responsive">
                            <table class="table table-hover table-stripped table-bordered">
                                <tbody>
                                <tr>
                                    <th colspan="2" class="text-center">INVOICE DETAILS</th>
                                </tr>
                                <tr>
                                    <th>Number</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Date Issued</th>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-stripped table-bordered">
                                <tbody>
                                <tr>
                                    <th colspan="2" class="text-center">CLIENT DETAILS</th>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Contact</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-stripped table-bordered">
                                <tbody>
                                <tr>
                                    <th colspan="4" class="text-center">SERVICES</th>
                                </tr>
                                <tr>
                                    <th>No</th>
                                    <th>DETAILS</th>
                                    <th>EACH</th>
                                    <th>AMOUNT</th>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Lorem</td>
                                    <td>Adult 4 Child 2</td>
                                    <td>4000 2000</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <th>TOTAL AMOUNT</th>
                                    <td>6000</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <th>RECEIVED</th>
                                    <td>3000</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <th>3000</th>
                                    <td>BALANCE</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-stripped table-bordered">
                                <tbody>
                                <tr>
                                    <td>This invoice is issued by</td>
                                    <td>Name</td>
                                    <td>On behalf of {{ config('app.name') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="table-responsive">
                            <table class="table table-hover table-stripped table-bordered">
                                <tbody>
                                <tr>
                                    <th colspan="2" class="text-center">BANK TRANSFER DETAILS</th>
                                </tr>
                                <tr>
                                    <th>Name A/C</th>
                                    <td>Firstname Lastname</td>
                                </tr>
                                <tr>
                                    <th>Correspondence Bank</th>
                                    <td>CRDB PLC</td>
                                </tr>
                                <tr>
                                    <th>Branch</th>
                                    <td>UDOM</td>
                                </tr>
                                <tr>
                                    <th>Account number</th>
                                    <td>222 2222 222 2222</td>
                                </tr>
                                <tr>
                                    <th>Swift Code</th>
                                    <td>CORU TZ 0000</td>
                                </tr>
                                <tr>
                                    <th>Main Code</th>
                                    <td>CORU TZ 0000</td>
                                </tr>
                                <tr>
                                    <td>Branch Code</td>
                                    <td>OT1</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>NB:</strong> At least 30% safari total amount shall be paid before to
                                        show commitment and
                                        reserved your
                                        safari; remain balance of 70% shall be clear upon arrival before safari start.
                                        Please e-mail proof of payment through
                                        <strong>Credit Card:</strong> Payment through online credit card please let us
                                        If you have any questions concerning this invoice, contact us with contacts
                                        below
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <p class="text-center">Thank you for choosing</p>

                        <h1 class="text-center">{{ config('app.name') }}</h1>

                        <p class="text-center">
                            Contact details
                        </p>

                    </div>
                    <div class="panel-footer">
                        <div class="text-center">
                            <a href="#" class="btn btn-primary"><i class="fa fa-envelope"></i> Send</a>
                            <a href="#" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
                        </div>
                    </div>
                </div>           
            </div>
          </div>

        </div>
        <div class="col-sm-2">
          @include('partials._right_sidebar')
        </div>
      </div>
    </div>

@endsection
