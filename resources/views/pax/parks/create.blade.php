@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials._left_sidebar')
        </div>
        <div class="col-sm-8">

          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                </div>
                <div class="panel-body text-center">
                  <div class="alert alert-warning">
                    Unconfirmed
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Services</h3>
                </div>
                <div class="panel-body">
                  @include('partials.pax._submenu')
                </div>
              </div>
            </div>
            <div class="col-sm-9">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Parks</h3>
                </div>
                
                    <div class="panel-body">

                        <h4 class="text-center">Which park does <strong>{{ $pax->name }}</strong> wants to visit?</h4>

                        {!! Form::open(['url' => 'dashboard/pax/'.$pax->id.'/parks', 'class' => 'form-horizontal']) !!}

                        @include('common.errors')

                        <div class="form-group">
                            {!! Form::label('park', 'Park', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('park', $parkList, null, ['class' => 'form-control', 'placeholder' => 'Pick a park']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('entries', 'Entries/Nights', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::number('entries', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('child', 'Child', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::number('child', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>                

                </div>
              </div>              
            </div>
          </div>

        </div>
        <div class="col-sm-2">
          @include('partials._right_sidebar')
        </div>
      </div>
    </div>

@endsection
