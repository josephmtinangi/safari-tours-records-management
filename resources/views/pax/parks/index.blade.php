@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials._left_sidebar')
        </div>
        <div class="col-sm-8">

          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">{{ $pax->name }}</h3>
                </div>
                <div class="panel-body text-center">
                  <div class="alert alert-warning">
                    Unconfirmed
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-3">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Services</h3>
                </div>
                <div class="panel-body">
                  @include('partials.pax._submenu')
                </div>
              </div>
            </div>
            <div class="col-sm-9">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Parks</h3>
                </div>
                <div class="panel-body">

                        @if($pax->parks->count() > 0)
                            <a href="{{ url('dashboard/pax/'.$pax->id.'/parks/new') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Park</th>
                                    <th>ppp</th>
                                    <th>Entries/Nights</th>
                                    <th>Child</th>
                                    <th colspan="2" class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pax->parks as $park)
                                    <tr>
                                        <td>{{ $park->id }}</td>
                                        <td>{{ $park->name }}</td>
                                        <td>{{ $park->ppp }}</td>
                                        <td>{{ $park->pivot->entries }}</td>
                                        <td>{{ $park->pivot->child }}</td>
                                        <td><a href="#">Edit</a></td>
                                        <td><a href="#">Delete</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @else
                            <div class="text-center">
                                <i class="fa fa-tree fa-5x"></i>
                                <h3>It seems like {{ $pax->name }} has not told you about the places to visit.</h3>
                                <a href="{{ url('dashboard/pax/'.$pax->id.'/parks/new') }}" class="btn btn-primary">Add New</a>
                            </div>
                        @endif

                </div>
              </div>              
            </div>
          </div>

        </div>
        <div class="col-sm-2">
          @include('partials._right_sidebar')
        </div>
      </div>
    </div>

@endsection
