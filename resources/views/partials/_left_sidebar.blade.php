<div class="list-group">
  <a href="{{ url('dashboard/pax') }}" class="list-group-item">Pax <span class="badge"><i class="fa fa-angle-right"></i></span></a>
  <a href="{{ url('dashboard/parks') }}" class="list-group-item">Parks <span class="badge"><i class="fa fa-angle-right"></i></span></a>
  <a href="{{ url('dashboard/accommodations') }}" class="list-group-item">Accommodations <span class="badge"><i class="fa fa-angle-right"></i></span></a>
  <a href="{{ url('dashboard/transports') }}" class="list-group-item">Transports <span class="badge"><i class="fa fa-angle-right"></i></span></a>
  <a href="{{ url('dashboard/extras') }}" class="list-group-item">Extras <span class="badge"><i class="fa fa-angle-right"></i></span></a>
  <a href="#" class="list-group-item">Item 3 <span class="badge"><i class="fa fa-angle-right"></i></span></a>
</div>