<div class="list-group">
  <a href="{{ url('dashboard/pax/'.$pax->id.'/parks') }}" class="list-group-item">Parks</a>
  <a href="{{ url('dashboard/pax/'.$pax->id.'/accommodations') }}" class="list-group-item">Accommodations</a>
  <a href="{{ url('dashboard/pax/'.$pax->id.'/transports') }}" class="list-group-item">Transports</a>
  <a href="{{ url('dashboard/pax/'.$pax->id.'/extras') }}" class="list-group-item">Extra Services</a>
  <a href="{{ url('dashboard/pax/'.$pax->id.'/itineraries') }}" class="list-group-item">Itineraries</a>
  <a href="{{ url('dashboard/pax/'.$pax->id.'/invoices') }}" class="list-group-item">Invoices</a>
  <a href="{{ url('dashboard/pax/'.$pax->id.'/billing') }}" class="list-group-item"><i class="fa fa-money"></i> Billing</a>
  <a href="#" class="list-group-item">Item 3</a>
</div>