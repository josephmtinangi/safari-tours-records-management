<div class="list-group">
    <a href="{{ url('dashboard/settings/company') }}" class="list-group-item"><i class="fa fa-flash fa-fw"></i> Profile</a>
    <a href="{{ url('dashboard/settings/company/bank-accounts') }}" class="list-group-item"><i
                class="fa fa-bank fa-fw"></i> Bank Transfer Details</a>
    <a href="{{ url('dashboard/account/profile') }}" class="list-group-item"><i class="fa fa-user fa-fw"></i> You</a>
</div>