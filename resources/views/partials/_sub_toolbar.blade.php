<div class="btn-toolbar">
    <div class="btn-group">
        <a href="#" class="btn btn-default btn-lg" data-toggle="tooltip"
           data-placement="top" title="Overview"><i class="fa fa-line-chart"></i></a>
        <a href="#" class="btn btn-default btn-lg" data-toggle="tooltip"
           data-placement="top" title="Transports"><i class="fa fa-car"></i></a>
        <a href="{{ url('dashboard/pax/' . $pax->id . '/parks') }}"
           class="btn btn-default btn-lg{{ Request::segment(4) == 'parks' ? ' active' : '' }}" data-toggle="tooltip"
           data-placement="top" title="Parks"><i class="fa fa-tree"></i></a>
        <a href="{{ url('dashboard/pax/' . $pax->id . '/accommodations') }}" class="btn btn-default btn-lg"
           data-toggle="tooltip"
           data-placement="top" title="Accommodations"><i class="fa fa-home"></i></a>
        <a href="#" class="btn btn-default btn-lg" data-toggle="tooltip"
           data-placement="top" title="Extras"><i class="fa fa-flash"></i></a>
        <a href="#" class="btn btn-default btn-lg" data-toggle="tooltip"
           data-placement="top" title="Itineraries"><i class="fa fa-file-excel-o"></i></a>
        <a href="{{ url('dashboard/pax/' . $pax->id . '/invoices') }}" class="btn btn-default btn-lg" data-toggle="tooltip"
           data-placement="top" title="Invoices"><i class="fa fa-book"></i></a>
        <a href="#" class="btn btn-default btn-lg"
           data-toggle="tooltip"
           data-placement="top" title="Invoices"><i
                    class="fa fa-book"></i></a>
    </div>
</div>