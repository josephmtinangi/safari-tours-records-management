<div class="container">
    <div class="row">
        <div class="col-md-12">

            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('dashboard') }}">Dashboard</a>
                </li>
                <li class="active">{{ ucfirst(Request::segment(2)) }}</li>
            </ol>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="col-sm-12">
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-default btn-lg"><i class="fa fa-dashboard"></i>
                            </button>
                            <a href="{{ url('dashboard/pax') }}"
                               class="btn btn-default btn-lg{{ Request::segment(2) == 'pax' ? ' active' : '' }}"
                               data-toggle="tooltip"
                               data-placement="top" title="Pax"><i class="fa fa-users"></i> Pax</a>
                            <a href="{{ url('dashboard/parks') }}"
                               class="btn btn-default btn-lg{{ Request::segment(2) == 'parks' ? ' active' : '' }}"
                               data-toggle="tooltip"
                               data-placement="top" title="Parks"><i
                                        class="fa fa-tree"></i> Parks</a>
                            <a href="#" class="btn btn-default btn-lg" data-toggle="tooltip"
                               data-placement="top" title="Transports"><i class="fa fa-car"></i> Transports</a>
                            <a href="{{ url('dashboard/accommodations') }}"
                               class="btn btn-default btn-lg {{ Request::segment(2) == 'accommodations' ? ' active' : '' }}"
                               data-toggle="tooltip"
                               data-placement="top" title="Accommodations"><i class="fa fa-home"></i> Accommodations</a>
                            <a href="{{ url('dashboard/extras') }}" class="btn btn-default btn-lg" data-toggle="tooltip"
                               data-placement="top" title="Extras"><i class="fa fa-flash"></i> Extras</a>
                            <a href="#" class="btn btn-default btn-lg" data-toggle="tooltip"
                               data-placement="top" title="Itineraries"><i class="fa fa-file-excel-o"></i>
                                Itineraries</a>
                            <a href="#" class="btn btn-default btn-lg" data-toggle="tooltip"
                               data-placement="top" title="Invoices"><i class="fa fa-book"></i> Invoices</a>
                        </div>

                        @if(Request::segment(2) == 'pax' && Request::segment(3) !== null && Request::segment(3) !== 'new')
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <h2>{{ $pax->name }}
                                        <small><a href="mailto:{{ $pax->email }}">{{ $pax->email }}</a></small>
                                    </h2>
                                    @include('partials._sub_toolbar')
                                </div>
                            </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>