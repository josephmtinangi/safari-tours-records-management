<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h4>MEET YOUR NEW {{ strtoupper(Config('app.name')) }}</h4>
                    <hr>
                    <ul class="list-unstyled">
                        <li><a href="#">Link 1</a></li>
                        <li><a href="#">Link 2</a></li>
                        <li><a href="#">Link 3</a></li>
                        <li><a href="#">Link 4</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h4>GET TO KNOW US</h4>
                    <hr>
                    <ul class="list-unstyled">
                        <li><a href="#">Link 1</a></li>
                        <li><a href="#">Link 2</a></li>
                        <li><a href="#">Link 3</a></li>
                        <li><a href="#">Link 4</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h4>CONNECT WITH US</h4>
                    <hr>
                    <ul class="list-unstyled">
                        <li><a href="#">Link 1</a></li>
                        <li><a href="#">Link 2</a></li>
                        <li><a href="#">Link 3</a></li>
                        <li><a href="#">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <i class="fa fa-atom fa-5x"></i> {{ config('app.name') }}

                    <div class="pull-right contact">
                        <span>+255000000</span>
                        <span><a href="mailto:appname@example.com" class="btn btn-primary">EMAIL SUPPORT</a></span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <span>&copy;{{ date('Y') }} {{ config('app.name') }}</span>
                    <div class="pull-right">
                        <a href="#">Support</a>
                        <a href="#">Status</a>
                        <a href="#">API</a>
                        <a href="#">Privacy</a>
                        <a href="#">Terms</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>