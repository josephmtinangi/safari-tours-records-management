@extends('layouts.app')

@section('page_title', 'About | ')

@section('content')
    <div class="jumbotron jumbotron-about">
        <div class="container text-center">
            <h1>We build the best records management software</h1>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="row">
                <h1 class="text-center">Overview</h1>
                <div class="col-sm-6">
                    Picture
                </div>
                <div class="col-sm-6">
                    <p>
                        Military-grade kanji dissident stimulate silent post-convenience store table receding.
                        Cyber-sunglasses nano-man beef noodles carbon denim neon courier rain. Range-rover tiger-team
                        motion drone construct face forwards modem narrative alcohol shrine man skyscraper city
                        disposable. Soul-delay render-farm physical tube motion Kowloon saturation point meta-uplink
                        table voodoo god bridge engine gang woman. Towards-ware convenience store decay modem
                        papier-mache dome garage shrine human pen rebar woman engine. Soul-delay pre-shoes woman modem
                        shrine cardboard. Numinous disposable gang futurity footage carbon dolphin woman engine camera
                        hotdog advert tanto DIY plastic.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <h1 class="text-center">Meet The Team</h1>

                <div class="col-sm-4">
                    <div class="thumbnail">
                        <img data-src="#" alt="Picture">
                        <div class="caption">
                            <h3>Name</h3>
                            <p>
                                Position
                            </p>
                            <p>
                                <a href="#" class="btn btn-primary"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-github"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-instagram"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-medium"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-bitbucket"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <img data-src="#" alt="Picture">
                        <div class="caption">
                            <h3>Name</h3>
                            <p>
                                Position
                            </p>
                            <p>
                                <a href="#" class="btn btn-primary"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-github"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-instagram"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-medium"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-bitbucket"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="thumbnail">
                        <img data-src="#" alt="Picture">
                        <div class="caption">
                            <h3>Name</h3>
                            <p>
                                Position
                            </p>
                            <p>
                                <a href="#" class="btn btn-primary"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-github"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-instagram"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-medium"></i></a>
                                <a href="#" class="btn btn-primary"><i class="fa fa-bitbucket"></i></a>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
