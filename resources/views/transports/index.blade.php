@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials._left_sidebar')
            </div>
            <div class="col-sm-8">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Transports</h3>
                    </div>
                    <div class="panel-body">

                        @if($transports->count() > 0)
                            <a href="{{ url('dashboard/transports/create') }}" class="btn btn-primary pull-right"><i
                                        class="fa fa-plus"></i> Add</a>
                            <br><br>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Name</th>
                                        <th>5 Seat Cost</th>
                                        <th>7 Seat Add</th>
                                        <th>Added</th>
                                        <th colspan="2" class="text-center"><i class="fa fa-cog"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transports as $transport)
                                        <tr>
                                            <td>{{ $transport->id }}</td>
                                            <td>{{ $transport->name }}</td>
                                            <td>{{ $transport->five_seat_cost }}</td>
                                            <td>{{ $transport->seven_seat_cost }}</td>
                                            <td>{{ $transport->created_at }}</td>
                                            <td><a href="{{ url('dashboard/transports/' . $transport->id . '/edit') }}"><i
                                                            class="fa fa-edit"></i></a></td>
                                            <td>
                                                <a data-toggle="modal"
                                                   href='#{{ $transport->id }}'><i class="fa fa-trash"></i></a>
                                                <div class="modal fade" id="{{ $transport->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">
                                                                    Deleting {{ $transport->name }}</h4>
                                                            </div>
                                                            <div class="modal-body text-center">
                                                                Deleting this transport is irreversible. <br>
                                                                Are you sure that you want to delete
                                                                <code>({{ $transport->name }}
                                                                    )</code>?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form action="{{ url('dashboard/transports/'.$transport->id) }}"
                                                                      method="POST">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('DELETE') }}

                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Cancel
                                                                    </button>
                                                                    <button type="submit"
                                                                            id="delete-task-{{ $transport->id }}"
                                                                            class="btn btn-danger">
                                                                        <i class="fa fa-btn fa-trash"></i> Delete
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="text-center">
                                <div class="text-center">
                                    <i class="fa fa-car fa-5x"></i>
                                    <h3>Looks like you don't provide any transport service. Start by adding new.</h3>
                                    <a href="{{ url('dashboard/transports/create') }}" class="btn btn-primary">Add</a>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
