@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">

                @include('partials.company._left_sidebar')

            </div>
            <div class="col-sm-8">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Company</h3>
                    </div>
                    <div class="panel-body">

                        @include('common.errors')

                        {!! Form::open(['url' => 'dashboard/settings/company', 'class' => 'form-horizontal', 'role' => 'form']) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', $company->name, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('slogan', 'Slogan', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('slogan', $company->slogan, ['class' => 'form-control']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            {!! Form::label('description', 'About', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::textarea('description', $company->description, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('address', 'Address', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('address', $company->address, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('tin', 'TIN', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('tin', $company->tin, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('website', 'Website', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('website', $company->website, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="primary_email" class="col-sm-3 control-label text-primary"><i
                                        class="fa fa-envelope fa-fw"></i> Primary Email</label>
                            <div class="col-sm-6">
                                {!! Form::email('primary_email', $company->primary_email, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="secondary_email" class="col-sm-3 control-label text-success"><i
                                        class="fa fa-envelope fa-fw"></i> Secondary Email</label>
                            <div class="col-sm-6">
                                {!! Form::email('secondary_email', $company->secondary_email, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="primary_phone" class="col-sm-3 control-label text-primary"><i
                                        class="fa fa-phone fa-fw"></i> Primary Phone</label>
                            <div class="col-sm-6">
                                {!! Form::text('primary_phone', $company->primary_phone, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="secondary_phone" class="col-sm-3 control-label text-success"><i
                                        class="fa fa-phone fa-fw"></i> Secondary Phone</label>
                            <div class="col-sm-6">
                                {!! Form::text('secondary_phone', $company->secondary_phone, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="skype" class="col-sm-3 control-label"><i
                                        class="fa fa-skype fa-fw"></i> Skype</label>
                            <div class="col-sm-6">
                                {!! Form::text('skype', $company->skype, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="facebook" class="col-sm-3 control-label"><i
                                        class="fa fa-facebook fa-fw"></i> Facebook</label>
                            <div class="col-sm-6">
                                {!! Form::text('facebook', $company->facebook, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="twitter" class="col-sm-3 control-label"><i
                                        class="fa fa-twitter fa-fw"></i> Twitter</label>
                            <div class="col-sm-6">
                                {!! Form::text('twitter', $company->twitter, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="instagram" class="col-sm-3 control-label"><i
                                        class="fa fa-instagram fa-fw"></i> Instagram</label>
                            <div class="col-sm-6">
                                {!! Form::text('instagram', $company->instagram, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="googleplus" class="col-sm-3 control-label"><i
                                        class="fa fa-google-plus fa-fw"></i> Google+</label>
                            <div class="col-sm-6">
                                {!! Form::text('googleplus', $company->googleplus, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="youtube" class="col-sm-3 control-label"><i
                                        class="fa fa-youtube fa-fw"></i> Youtube</label>
                            <div class="col-sm-6">
                                {!! Form::text('youtube', $company->youtube, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="linkedin" class="col-sm-3 control-label"><i
                                        class="fa fa-linkedin fa-fw"></i> LinkedIn</label>
                            <div class="col-sm-6">
                                {!! Form::text('linkedin', $company->linkedin, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="other" class="col-sm-3 control-label"><i
                                        class="fa fa-flask fa-fw"></i> Other</label>
                            <div class="col-sm-6">
                                {!! Form::text('other', $company->other, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
