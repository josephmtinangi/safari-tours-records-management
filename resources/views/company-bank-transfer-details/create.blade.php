@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">

                @include('partials.company._left_sidebar')

            </div>
            <div class="col-sm-8">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Bank Transfer Details</h3>
                    </div>
                    <div class="panel-body">

                        {!! Form::open(['url' => 'dashboard/settings/company/bank-accounts', 'class' => 'form-horizontal', 'role' => 'form']) !!}

                        @include('company-bank-transfer-details._form')

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
