@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">

                @include('partials.company._left_sidebar')

            </div>
            <div class="col-sm-8">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Bank Transfer Details</h3>
                    </div>
                    <div class="panel-body">

                        @if($company_bank_transfer_details->count() > 0)

                            <a href="{{ url('dashboard/settings/company/bank-accounts/create') }}"
                               class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                            <hr>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Account Name</th>
                                        <th>Correspondence Bank</th>
                                        <th>Branch</th>
                                        <th>Account Number</th>
                                        <th>Swift Code</th>
                                        <th>Main Code</th>
                                        <th>Branch Code</th>
                                        <th class="text-center" colspan="2"><i class="fa fa-cogs"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($company_bank_transfer_details as $bank_transfer_detail)
                                        <tr>
                                            <td>{{ $bank_transfer_detail->id }}</td>
                                            <td>{{ $bank_transfer_detail->account_name }}</td>
                                            <td>{{ $bank_transfer_detail->correspondence_bank }}</td>
                                            <td>{{ $bank_transfer_detail->branch }}</td>
                                            <td>{{ $bank_transfer_detail->account_number }}</td>
                                            <td>{{ $bank_transfer_detail->swift_code }}</td>
                                            <td>{{ $bank_transfer_detail->main_code }}</td>
                                            <td>{{ $bank_transfer_detail->branch_code }}</td>
                                            <td>
                                                <a href="{{ url('dashboard/settings/company/bank-accounts/'.$bank_transfer_detail->id.'/edit') }}"><i
                                                            class="fa fa-edit"></i></a></td>
                                            <td><a href="#"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        @else
                            <div class="text-center">
                                <i class="fa fa-bank fa-5x"></i>
                                <h3>No bank transfer detail added yet.</h3>
                                <a href="{{ url('dashboard/settings/company/bank-accounts/create') }}"
                                   class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                            </div>
                        @endif

                    </div>
                </div>

            </div>
            <div class="col-sm-2">
                @include('partials._right_sidebar')
            </div>
        </div>
    </div>

@endsection
