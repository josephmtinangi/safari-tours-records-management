@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials._left_sidebar')
        </div>
        <div class="col-sm-8">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title text-center">Parks</h3>
            </div>
<div class="panel-body">
                        @if($parks->count() > 0)
                            <a href="{{ url('dashboard/parks/new') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>
                            <br><br>
                           <div class="table-responsive">
                               <table class="table table-striped table-hover table-bordered">
                                   <thead>
                                   <tr>
                                       <th>SN</th>
                                       <th>Name</th>
                                       <th>PPP</th>
                                       <th colspan="2" class="text-center">Actions</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   @foreach($parks as $park)
                                       <tr>
                                           <td>{{ $park->id }}</td>
                                           <td>{{ $park->name }}</td>
                                           <td>{{ $park->ppp }}</td>
                                           <td><a href="{{ url('dashboard/parks/' . $park->id . '/edit') }}"><i class="fa fa-edit"></i></a></td>
                                           <!-- Delete Button -->
                                           <td>
                                               <a data-toggle="modal"
                                                  href='#{{ $park->id }}'><i class="fa fa-trash"></i></a>
                                               <div class="modal fade" id="{{ $park->id }}">
                                                   <div class="modal-dialog">
                                                       <div class="modal-content">
                                                           <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal"
                                                                       aria-hidden="true">&times;</button>
                                                               <h4 class="modal-title">Deleting {{ $park->name }}</h4>
                                                           </div>
                                                           <div class="modal-body">
                                                               Deleting a park is irreversible. <br>
                                                               Are you sure that you want to delete <code>({{ $park->name }}
                                                                   )</code>?
                                                           </div>
                                                           <div class="modal-footer">
                                                               <form action="{{ url('dashboard/parks/'.$park->id) }}"
                                                                     method="POST">
                                                                   {{ csrf_field() }}
                                                                   {{ method_field('DELETE') }}

                                                                   <button type="button" class="btn btn-default"
                                                                           data-dismiss="modal">Cancel
                                                                   </button>
                                                                   <button type="submit" id="delete-task-{{ $park->id }}"
                                                                           class="btn btn-danger">
                                                                       <i class="fa fa-btn fa-trash"></i> Delete
                                                                   </button>
                                                               </form>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </td>
                                       </tr>
                                   @endforeach
                                   </tbody>
                               </table>
                           </div>
                        @else
                            <div class="text-center">
                                <i class="fa fa-tree fa-5x"></i>
                                <h3>Looks like there are no parks for your guests to visit. Add new now.</h3>
                                <a href="{{ url('dashboard/parks/new') }}" class="btn btn-primary">Add New</a>
                            </div>
                        @endif
                    </div>
          </div>

        </div>
        <div class="col-sm-2">
          @include('partials._right_sidebar')
        </div>
      </div>
    </div>

@endsection
