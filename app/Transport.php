<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'five_seat_cost',
        'seven_seat_cost',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
