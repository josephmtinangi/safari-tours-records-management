<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{
    protected $fillable = [
        'user_id',
        'paxis_id',
        'adults',
        'children',
        'duration_days',
        'duration_nights',
        'status',
        'date_starts',
        'date_ends',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
