<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paxis extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'phone',
        'nationality',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function parks()
    {
        return $this->belongsToMany('App\Park')->withPivot('entries', 'child', 'user_id');
    }

    public function accommodations()
    {
        return $this->belongsToMany('App\Accommodation')->withPivot('nights');
    }

    public function extras()
    {
        return $this->belongsToMany('App\Extra')->withPivot('uses');
    }

    public function itineraries()
    {
        return $this->hasMany('App\Itinerary');
    }

    public function transports()
    {
        return $this->belongsToMany('App\Transport');
    }
}
