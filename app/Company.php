<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $fillable = [
        'user_id',
        'name',
        'slug',
        'slogan',
        'description',
        'tin',
        'address',
        'website',
        'primary_email',
        'secondary_email',
        'primary_phone',
        'secondary_phone',
        'skype',
        'facebook',
        'twitter',
        'instagram',
        'googleplus',
        'youtube',
        'linkedin',
        'other',
    ];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function bank_transfer_details()
    {
        return $this->hasMany('App\CompanyBankTransferDetail');
    }
}
