<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extra extends Model
{
    protected $fillable = [
        'name',
        'description',
        'cost',
        'iterations',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pax()
    {
        return $this->belongsToMany('App\Paxis')->withPivot('uses');
    }
}
