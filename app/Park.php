<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Park extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'image',
        'ppp',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function paxes()
    {
        return $this->belongsToMany('App\Paxis')->withPivot('entries', 'child', 'user_id');
    }
}
