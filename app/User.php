<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function paxes()
    {
        return $this->hasMany('App\Paxis');
    }

    public function parks()
    {
        return $this->hasMany('App\Park');
    }

    public function accommodations()
    {
        return $this->hasMany('App\Accommodation');
    }

    public function extras()
    {
        return $this->hasMany('App\Extra');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function itineraries()
    {
        return $this->hasMany('App\Itinerary');
    }

    public function company()
    {
        return $this->hasOne('App\Company');
    }

    public function transports()
    {
        return $this->hasMany('App\Transport');
    }
}
