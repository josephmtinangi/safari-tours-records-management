<?php

namespace App\Http\Controllers;

use App\Paxis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillingController extends Controller
{
    public function show($id)
    {
        $pax = Paxis::whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('billing.show', compact('pax'));
    }
}
