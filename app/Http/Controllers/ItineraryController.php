<?php

namespace App\Http\Controllers;

use App\Itinerary;
use App\Paxis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItineraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // validate
        $this->validate($request, [
            'adults' => 'required|integer',
            'children' => 'required|integer',
            'duration_days' => 'required|integer',
            'duration_nights' => 'required|integer',
            'date_starts' => 'required|date',
            'date_ends' => 'required|date',
        ]);

        // get user
        $user = Auth::user();
        // get pax
        $pax = Paxis::whereId($id)->whereUserId($user->id)->first();

        // check pax
        if ($pax) {

            $itinerary = new Itinerary();
            $itinerary->user_id = $user->id;
            $itinerary->paxis_id = $pax->id;
            $itinerary->adults = $request->input('adults');
            $itinerary->children = $request->input('children');
            $itinerary->status = $request->input('status');
            $itinerary->duration_days = $request->input('duration_days');
            $itinerary->duration_nights = $request->input('duration_nights');
            $itinerary->date_starts = $request->input('date_starts');
            $itinerary->date_ends = $request->input('date_ends');
            $itinerary->save();

            flash('Itinerary generated successfully', 'success');
            return redirect('dashboard/pax/' . $pax->id . '/itineraries');
        }

        flash('Itinerary was not generated.', 'danger');

        return redirect('dashboard/pax/' . $pax->id . '/itineraries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
