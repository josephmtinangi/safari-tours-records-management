<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showCompanyProfile()
    {
        $user = Auth::user();
        $company = $user->company;
        if (!$company) {
            $company = new Company();

            $company->name = $user->name;
            $company->slug = str_slug($user->name, '-');

            $user->company()->save($company);
        }
        return view('settings.company.profile', compact('company'));
    }

    public function storeCompanyProfile(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'name' => 'required',
        ]);

        $company = $user->company;
        $company->name = $request->input('name');
        $company->slug = str_slug($request->input('name'), '-');
        $company->slogan = $request->input('slogan');
        $company->description = $request->input('description');
        $company->address = $request->input('address');
        $company->tin = $request->input('tin');
        $company->website = $request->input('website');

        $company->primary_email = $request->input('primary_email');
        $company->secondary_email = $request->input('secondary_email');
        $company->primary_phone = $request->input('primary_phone');
        $company->secondary_phone = $request->input('secondary_phone');

        $company->skype = $request->input('skype');
        $company->facebook = $request->input('facebook');
        $company->twitter = $request->input('twitter');
        $company->instagram = $request->input('instagram');
        $company->googleplus = $request->input('googleplus');
        $company->youtube = $request->input('youtube');
        $company->linkedin = $request->input('linkedin');
        $company->other = $request->input('other');

        $user->company()->save($company);

        flash('Updated successfully.', 'success');

        return redirect('dashboard/settings/company');
    }
}
