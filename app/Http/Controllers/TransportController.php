<?php

namespace App\Http\Controllers;

use App\Transport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $transports = $user->transports;
        return view('transports.index', compact('transports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'name' => 'required',
            'five_seat_cost' => 'numeric',
            'seven_seat_cost' => 'numeric',
        ]);

        $transport = new Transport();
        $transport->name = $request->input('name');
        $transport->five_seat_cost = $request->input('five_seat_cost');
        $transport->seven_seat_cost = $request->input('seven_seat_cost');

        $user->transports()->save($transport);

        flash('Saved successfully.');

        return redirect('dashboard/transports');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $transport = Transport::whereId($id)->whereUserId($user->id)->first();
        return view('transports.edit', compact('transport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $this->validate($request, [
            'name' => 'required',
            'five_seat_cost' => 'numeric',
            'seven_seat_cost' => 'numeric',
        ]);

        $transport = Transport::whereId($id)->whereUserId($user->id)->first();
        $transport->name = $request->input('name');
        $transport->five_seat_cost = $request->input('five_seat_cost');
        $transport->seven_seat_cost = $request->input('seven_seat_cost');

        $user->transports()->save($transport);

        flash('Updated successfully.');

        return redirect('dashboard/transports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $transport = Transport::whereId($id)->whereUserId($user->id)->first();

        if ($transport) {
            $transport->delete();
            flash('Transport deleted successfully.', 'success');
            return redirect('dashboard/transports');
        }

        flash('Could not delete the transport', 'warning');
        return redirect('dashboard/transports');
    }
}
