<?php

namespace App\Http\Controllers;

use App\Accommodation;
use App\Itinerary;
use App\Park;
use App\Paxis;
use App\Transport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Extra;

class PaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paxes = Paxis::whereUserId(Auth::user()->id)->paginate(10);
        return view('pax.index', compact('paxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pax.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $user = Auth::user();

        $pax = new Paxis();
        $pax->name = $request->input('name');
        $pax->email = $request->input('email');
        $pax->phone = $request->input('phone');
        $pax->nationality = $request->input('nationality');

        $user->paxes()->save($pax);

        flash('Pax created successfully.', 'success');

        return redirect('dashboard/pax/' . $pax->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pax = Paxis::whereId($id)->whereUserId(Auth::user()->id)->first();
        if ($pax) {
            return view('pax.show', compact('pax'));
        }

        return redirect('dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pax = Paxis::whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.edit', compact('pax'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $user = Auth::user();

        $pax = Paxis::whereId($id)->whereUserId($user->id)->first();
        $pax->name = $request->input('name');
        $pax->email = $request->input('email');
        $pax->phone = $request->input('phone');
        $pax->nationality = $request->input('nationality');

        $pax->save();

        flash($pax->name . ' updated successfully.', 'success');

        return redirect('dashboard/pax');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $pax = Paxis::whereId($id)->whereUserId($user->id)->first();

        if ($pax) {
            $pax->delete();

            flash('Pax deleted successfully.', 'success');

            return redirect('dashboard/pax');
        }

        flash('Could not delete the pax', 'warning');

        return redirect('dashboard/pax');
    }

    public function parks($id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.parks.index', compact('pax'));
    }

    public function createPark($id)
    {
        $user = Auth::user();
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();
        $parkList = $user->parks->pluck('name', 'id');
        return view('pax.parks.create', compact('pax', 'parkList'));
    }

    public function storePark(Request $request, $id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();

        $this->validate($request, [
            'park' => 'required',
            'entries' => 'required',
        ]);

        $park = Park::whereId($request->input('park'))->whereUserId(Auth::user()->id)->first();

        if ($park) {
            $pax->parks()->save($park, ['entries' => $request->input('entries'), 'child' => $request->input('child'), 'user_id' => Auth::user()->id]);

            flash('Park added successfully', 'success');
            return redirect('dashboard/pax/' . $pax->id . '/parks');
        }

        flash('Could not add a park', 'warning');

        return redirect('dashboard/pax/' . $pax->id . '/parks');
    }

    public function accommodations($id)
    {
        $pax = Paxis::with('accommodations')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.accommodations.index', compact('pax'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createAccommodation($id)
    {
        $user = Auth::user();
        $pax = Paxis::with('accommodations')->whereId($id)->whereUserId(Auth::user()->id)->first();
        $accommodationList = $user->accommodations->pluck('name', 'id');
        return view('pax.accommodations.create', compact('pax', 'accommodationList'));
    }

    public function storeAccommodation(Request $request, $id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();

        $this->validate($request, [
            'accommodation' => 'required',
            'nights' => 'required',
        ]);

        $accommodation = Accommodation::whereId($request->input('accommodation'))->whereUserId(Auth::user()->id)->first();

        if ($accommodation) {

            $pax->accommodations()->save($accommodation, ['nights' => $request->input('nights'), 'user_id' => Auth::user()->id]);

            flash('Accommodation added successfully', 'success');
            return redirect('dashboard/pax/' . $pax->id . '/accommodations');
        }

        flash('Could not add accommodation', 'warning');

        return redirect('dashboard/pax/' . $pax->id . '/accommodations');
    }

    public function transports($id)
    {
        $pax = Paxis::with('transports')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.transports.index', compact('pax'));
    }

    public function createTransport($id)
    {
        $user = Auth::user();
        $pax = Paxis::with('accommodations')->whereId($id)->whereUserId(Auth::user()->id)->first();
        $transportList = $user->transports->pluck('name', 'id');
        return view('pax.transports.create', compact('pax', 'transportList'));
    }

    public function storeTransport(Request $request, $id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();

        $this->validate($request, [
            'transport' => 'required',
        ]);

        $transport = Transport::whereId($request->input('transport'))->whereUserId(Auth::user()->id)->first();

        if ($transport) {

            $pax->transports()->save($transport, ['user_id' => Auth::user()->id]);

            flash('Transport added successfully', 'success');
            return redirect('dashboard/pax/' . $pax->id . '/transports');
        }

        flash('Could not add transports', 'warning');

        return redirect('dashboard/pax/' . $pax->id . '/transports');
    }

    public function showInvoice($id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.invoices.show', compact('pax'));
    }

    public function invoices($id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.invoices.index', compact('pax'));
    }

    public function showItinerary($pax_id, $itinerary_id)
    {
        $pax = Paxis::with('itineraries')->whereId($pax_id)->whereUserId(Auth::user()->id)->first();
        $itinerary = Itinerary::whereId($itinerary_id)->whereUserId(Auth::user()->id)->wherePaxisId($pax->id)->first();
        return view('pax.itineraries.show', compact('pax', 'itinerary'));
    }

    public function itineraries($id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.itineraries.index', compact('pax'));
    }

    public function createItinerary($id)
    {
        $pax = Paxis::with('parks')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.itineraries.create', compact('pax'));
    }

    public function extras($id)
    {
        $pax = Paxis::with('extras')->whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('pax.extras.index', compact('pax'));
    }

    public function createExtra($id)
    {
        $pax = Paxis::with('extras')->whereId($id)->whereUserId(Auth::user()->id)->first();
        $user = Auth::user();
        $extraList = $user->extras->pluck('name', 'id');
        return view('pax.extras.create', compact('pax', 'extraList'));
    }

    public function storeExtra(Request $request, $id)
    {

        $this->validate($request, [
            'extra' => 'required',
            'uses' => 'required',
        ]);

        $user = Auth::user();

        $pax = Paxis::with('extras')->whereId($id)->whereUserId($user->id)->first();

        $extra = Extra::whereId($request->input('extra'))->whereUserId($user->id)->first();

        if ($extra) {
            $pax->extras()->save($extra, ['uses' => $request->input('uses'), 'user_id' => $user->id]);

            flash('Service added successfully.', 'success');
            return redirect('dashboard/pax/' . $pax->id . '/extras');
        }

        flash('Could not add the service', 'danger');
        return redirect('dashboard/pax/' . $pax->id . '/extras');
    }
}
