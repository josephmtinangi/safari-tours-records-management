<?php

namespace App\Http\Controllers;

use App\Extra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExtraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extras = Auth::user()->extras()->get();
        return view('extras.index', compact('extras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('extras.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required',
        ]);

        $user = Auth::user();

        $extra = new Extra();
        $extra->name = $request->input('name');
        $extra->description = $request->input('description');
        $extra->cost = $request->input('cost');

        $user->extras()->save($extra);

        flash('Extra added successfully.', 'success');

        return redirect('dashboard/extras');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $extra = Extra::whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('extras.edit', compact('extra'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required',
        ]);


        $extra = Extra::whereId($id)->whereUserId(Auth::user()->id)->first();
        $extra->name = $request->input('name');
        $extra->description = $request->input('description');
        $extra->cost = $request->input('cost');
        $extra->save();


        flash($extra->name . ' edited successfully.', 'success');

        return redirect('dashboard/extras');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $extra = Extra::whereId($id)->whereUserId($user->id)->first();

        if ($extra) {
            $extra->delete();
            flash('Extra deleted successfully.', 'success');
            return redirect('dashboard/extras');
        }

        flash('Could not delete the extra', 'warning');
        return redirect('dashboard/extras');
    }
}
