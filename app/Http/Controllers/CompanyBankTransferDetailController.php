<?php

namespace App\Http\Controllers;

use App\CompanyBankTransferDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyBankTransferDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $company = $user->company;
        $company_bank_transfer_details = $company->bank_transfer_details;

        return view('company-bank-transfer-details.index', compact('company_bank_transfer_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company-bank-transfer-details.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'account_name' => 'required',
            'account_number' => 'required',
        ]);

        $user = Auth::user();
        $company = $user->company;

        $company_bank_transfer_detail = new CompanyBankTransferDetail();
        $company_bank_transfer_detail->account_name = $request->input('account_name');
        $company_bank_transfer_detail->correspondence_bank = $request->input('correspondence_bank');
        $company_bank_transfer_detail->account_number = $request->input('account_number');
        $company_bank_transfer_detail->branch = $request->input('branch');
        $company_bank_transfer_detail->swift_code = $request->input('swift_code');
        $company_bank_transfer_detail->main_code = $request->input('main_code');
        $company_bank_transfer_detail->branch_code = $request->input('branch_code');
        $company_bank_transfer_detail->notes = $request->input('notes');

        $company->bank_transfer_details()->save($company_bank_transfer_detail);

        flash('Added successfully', 'success');

        return redirect('dashboard/settings/company/bank-accounts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $company = $user->company;
        $company_bank_transfer_detail = CompanyBankTransferDetail::whereId($id)->whereCompanyId($company->id)->first();

        return view('company-bank-transfer-details.edit', compact('company_bank_transfer_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'account_name' => 'required',
            'account_number' => 'required',
        ]);

        $user = Auth::user();
        $company = $user->company;

        $company_bank_transfer_detail = CompanyBankTransferDetail::whereId($id)->whereCompanyId($company->id)->first();

        $company_bank_transfer_detail->account_name = $request->input('account_name');
        $company_bank_transfer_detail->correspondence_bank = $request->input('correspondence_bank');
        $company_bank_transfer_detail->account_number = $request->input('account_number');
        $company_bank_transfer_detail->branch = $request->input('branch');
        $company_bank_transfer_detail->swift_code = $request->input('swift_code');
        $company_bank_transfer_detail->main_code = $request->input('main_code');
        $company_bank_transfer_detail->branch_code = $request->input('branch_code');
        $company_bank_transfer_detail->notes = $request->input('notes');

        $company->bank_transfer_details()->save($company_bank_transfer_detail);

        flash('Updated successfully', 'success');

        return redirect('dashboard/settings/company/bank-accounts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
