<?php

namespace App\Http\Controllers;

use App\Park;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ParkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $parks = Park::whereUserId($user->id)->paginate(10);
        return view('parks.index', compact('parks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'ppp' => 'required',
        ]);

        $user = Auth::user();

        $park = new Park();
        $park->name = $request->input('name');
        $park->description = $request->input('description');
        $park->ppp = $request->input('ppp');
        $user->parks()->save($park);


        flash('Park created successfully.', 'success');

        return redirect('dashboard/parks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $park = Park::whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('parks.edit', compact('park'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'ppp' => 'required',
        ]);

        $park = Park::whereId($id)->whereUserId(Auth::user()->id)->first();
        $park->name = $request->input('name');
        $park->description = $request->input('description');
        $park->ppp = $request->input('ppp');
        $park->save();


        flash('Park updated successfully.', 'success');

        return redirect('dashboard/parks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $park = Park::whereId($id)->whereUserId($user->id)->first();

        if($park){
            $park->delete();
            flash('Park deleted successfully.', 'success');
            return redirect('dashboard/parks');
        }

        flash('Could not delete the park', 'warning');
        return redirect('dashboard/parks');
    }
}
