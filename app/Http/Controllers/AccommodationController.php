<?php

namespace App\Http\Controllers;

use App\Accommodation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccommodationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accommodations = Accommodation::whereUserId(Auth::user()->id)->paginate(10);
        return view('accommodations.index', compact('accommodations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('accommodations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'pps' => 'required',
        ]);

        $user = Auth::user();
        $accommodation = new Accommodation();
        $accommodation->name = $request->input('name');
        $accommodation->description = $request->input('description');
        $accommodation->pps = $request->input('pps');
        $user->accommodations()->save($accommodation);

        flash('Accommodation added successfully.', 'success');

        return redirect('dashboard/accommodations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accommodation = Accommodation::whereId($id)->whereUserId(Auth::user()->id)->first();
        return view('accommodations.edit', compact('accommodation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'pps' => 'required',
        ]);

        $user = Auth::user();
        $accommodation = Accommodation::whereId($id)->whereUserId(Auth::user()->id)->first();
        $accommodation->name = $request->input('name');
        $accommodation->description = $request->input('description');
        $accommodation->pps = $request->input('pps');
        $user->accommodations()->save($accommodation);

        flash('Accommodation updated successfully.', 'success');

        return redirect('dashboard/accommodations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $accommodation = Accommodation::whereId($id)->whereUserId($user->id)->first();

        if($accommodation){
            $accommodation->delete();
            flash('Accommodation deleted successfully.', 'success');
            return redirect('dashboard/accommodations');
        }

        flash('Could not delete the accommodation', 'warning');
        return redirect('dashboard/accommodations');
    }
}
