<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'pps',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function paxes()
    {
        return $this->belongsToMany('App\Paxis')->withPivot('nights');
    }
}
