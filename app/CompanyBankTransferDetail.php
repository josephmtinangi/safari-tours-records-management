<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyBankTransferDetail extends Model
{
    protected $fillable = [
        'company_id',
        'company_id',

        'account_name',
        'account_number',
        'correspondence_bank',
        'branch',

        'swift_code',
        'main_code',
        'branch_code', 'notes',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
