<?php

use Illuminate\Database\Seeder;

class PaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paxes')->delete();
        factory(App\Paxis::class, 10)->create();
    }
}
