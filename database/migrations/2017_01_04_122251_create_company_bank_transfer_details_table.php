<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyBankTransferDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_bank_transfer_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onUpdate('cascade');

            $table->string('account_name');
            $table->string('account_number');
            $table->string('correspondence_bank')->nullable();
            $table->string('branch');

            $table->string('swift_code')->nullable();
            $table->string('main_code')->nullable();
            $table->string('branch_code')->nullable();

            $table->text('notes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_bank_transfer_details');
    }
}
