<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkPaxisPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('park_paxis', function (Blueprint $table) {
            $table->integer('park_id')->unsigned()->index();
            $table->foreign('park_id')->references('id')->on('parks')->onDelete('cascade');
            $table->integer('paxis_id')->unsigned()->index();
            $table->foreign('paxis_id')->references('id')->on('paxes')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('entries')->default(1);
            $table->integer('child')->default(0);
            $table->primary(['park_id', 'paxis_id', 'user_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('park_paxis');
    }
}
