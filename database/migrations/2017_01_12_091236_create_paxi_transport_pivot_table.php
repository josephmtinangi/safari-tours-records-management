<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaxiTransportPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paxis_transport', function (Blueprint $table) {
            $table->integer('paxis_id')->unsigned()->index();
            $table->foreign('paxis_id')->references('id')->on('paxes')->onDelete('cascade');
            $table->integer('transport_id')->unsigned()->index();
            $table->foreign('transport_id')->references('id')->on('transports')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->primary(['paxis_id', 'transport_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paxi_transport');
    }
}
