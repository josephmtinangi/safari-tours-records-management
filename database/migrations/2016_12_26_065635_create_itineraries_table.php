<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itineraries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('paxis_id')->unsigned()->index();
            $table->foreign('paxis_id')->references('id')->on('paxes')->onDelete('cascade');
            $table->integer('adults')->default(1);
            $table->integer('children')->default(0);
            $table->integer('duration_days');
            $table->integer('duration_nights');
            $table->boolean('status')->default(false);
            $table->timestamp('date_starts');
            $table->timestamp('date_ends');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itineraries');
    }
}
