<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('dashboard', 'HomeController@index');

Route::get('dashboard/pax', 'PaxController@index');
Route::get('dashboard/pax/new', 'PaxController@create');
Route::get('dashboard/pax/{id}', 'PaxController@show');
Route::post('dashboard/pax', 'PaxController@store');
Route::get('dashboard/pax/{id}/edit', 'PaxController@edit');
Route::patch('dashboard/pax/{id}', 'PaxController@update');
Route::delete('dashboard/pax/{id}', 'PaxController@destroy');


Route::get('dashboard/pax/{id}/parks', 'PaxController@parks');
Route::get('dashboard/pax/{id}/parks/new', 'PaxController@createPark');
Route::post('dashboard/pax/{id}/parks', 'PaxController@storePark');


Route::get('dashboard/parks', 'ParkController@index');
Route::get('dashboard/parks/new', 'ParkController@create');
Route::post('dashboard/parks', 'ParkController@store');
Route::get('dashboard/parks/{id}/edit', 'ParkController@edit');
Route::patch('dashboard/parks/{id}', 'ParkController@update');
Route::delete('dashboard/parks/{id}', 'ParkController@destroy');

Route::get('dashboard/accommodations', 'AccommodationController@index');
Route::get('dashboard/accommodations/new', 'AccommodationController@create');
Route::post('dashboard/accommodations', 'AccommodationController@store');
Route::get('dashboard/accommodations/{id}/edit', 'AccommodationController@edit');
Route::patch('dashboard/accommodations/{id}', 'AccommodationController@update');
Route::delete('dashboard/accommodations/{id}', 'AccommodationController@destroy');

Route::get('pricing', 'PageController@pricing');
Route::get('about', 'PageController@about');

Route::get('dashboard/pax/{id}/accommodations', 'PaxController@accommodations');
Route::get('dashboard/pax/{id}/accommodations/new', 'PaxController@createAccommodation');
Route::post('dashboard/pax/{id}/accommodations', 'PaxController@storeAccommodation');

Route::get('dashboard/pax/{id}/transports', 'PaxController@transports');
Route::get('dashboard/pax/{id}/transports/new', 'PaxController@createTransport');
Route::post('dashboard/pax/{id}/transports', 'PaxController@storeTransport');

Route::get('dashboard/extras', 'ExtraController@index');
Route::get('dashboard/extras/new', 'ExtraController@create');
Route::post('dashboard/extras', 'ExtraController@store');
Route::get('dashboard/extras/{id}/edit', 'ExtraController@edit');
Route::patch('dashboard/extras/{id}', 'ExtraController@update');
Route::delete('dashboard/extras/{id}', 'ExtraController@destroy');

Route::get('dashboard/pax/{id}/invoices', 'PaxController@invoices');
Route::get('dashboard/pax/{id}/invoices/1', 'PaxController@showInvoice');

Route::get('dashboard/pax/{id}/itineraries/new', 'PaxController@createItinerary');
Route::get('dashboard/pax/{id}/itineraries', 'PaxController@itineraries');
Route::get('dashboard/pax/{pax_id}/itineraries/{itinerary_id}', 'PaxController@showItinerary');

Route::get('dashboard/pax/{id}/extras', 'PaxController@extras');
Route::get('dashboard/pax/{id}/extras/new', 'PaxController@createExtra');
Route::post('dashboard/pax/{id}/extras', 'PaxController@storeExtra');

Route::get('dashboard/account/profile', 'UserController@profile');
Route::post('dashboard/account/profile/user', 'UserController@storeUser');

Route::post('dashboard/pax/{id}/itinerary', 'ItineraryController@store');

Route::get('dashboard/settings/company', 'SettingsController@showCompanyProfile');
Route::post('dashboard/settings/company', 'SettingsController@storeCompanyProfile');

Route::get('dashboard/pax/{id}/billing', 'BillingController@show');

Route::resource('dashboard/settings/company/bank-accounts', 'CompanyBankTransferDetailController');

Route::resource('dashboard/transports', 'TransportController');